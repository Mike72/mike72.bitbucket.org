
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var genError;
function generirajPodatke(stPacienta, callback) {
    var ime;
    var priimek;
    var spol;
    var datumRojstva;
    var visina;
    var teza;
    var sisTlak;
    var diaTlak;
    var datumInUra = new Date().toISOString();
    
    switch(stPacienta) {
        case 1:
            ime = "Mija";
            priimek = "Novak";
            spol = "FEMALE";
            datumRojstva = "1986-04-16T00:00:00.000Z";
            visina = 164;
            teza = 57.8;
            sisTlak = 117;
            diaTlak = 83;
            break;
        case 2:
            ime = "Franc";
            priimek = "Petek";
            spol = "MALE";
            datumRojstva = "1994-06-18T00:00:00.000Z";
            visina = 172;
            teza = 84.6;
            sisTlak = 132;
            diaTlak = 88;
            break;
        case 3:
            ime = "Marjeta";
            priimek = "Golob";
            spol = "FEMALE";
            datumRojstva = "1974-03-29T00:00:00.000Z";
            visina = 168;
            teza = 48.7;
            sisTlak = 93;
            diaTlak = 72;
            break;
    }
    
    var ehrId = "";
    var sessionId = getSessionId();
    setTimeout(function (){

        $.ajaxSetup({
    	    headers: {"Ehr-Session": sessionId}
    	});
    	$.ajax({
    	    url: baseUrl + "/ehr",
    	    type: 'POST',
    	    success: function (data) {
    	        var ehrId = data.ehrId;
    	        var partyData = {
    	            firstNames: ime,
    	            lastNames: priimek,
    	            gender: spol,
    	            dateOfBirth: datumRojstva,
    	            partyAdditionalInfo: [{
    	                key: "ehrId", 
    	                value: ehrId
    	            }]
    	        };
    	        $.ajax({
    	            url: baseUrl + "/demographics/party",
    	            type: 'POST',
    	            contentType: 'application/json',
    	            data: JSON.stringify(partyData),
    	            success: function (party) {
    	                if (party.action == 'CREATE') {
    	                   $.ajaxSetup({
                    		    headers: {"Ehr-Session": sessionId}
                    		});
                    		var podatki = {
                    		    "ctx/language": "en",
                    		    "ctx/territory": "SI",
                    		    "ctx/time": datumInUra,
                    		    "vital_signs/height_length/any_event/body_height_length": visina,
                    		    "vital_signs/body_weight/any_event/body_weight": teza,
                    		    "vital_signs/blood_pressure/any_event/systolic": sisTlak,
                                "vital_signs/blood_pressure/any_event/diastolic": diaTlak
                    		};
                    		var parametriZahteve = {
                    		    ehrId: ehrId,
                    		    templateId: 'Vital Signs',
                    		    format: 'FLAT',
                    		};
                    		$.ajax({
                    		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
                    		    type: 'POST',
                    		    contentType: 'application/json',
                    		    data: JSON.stringify(podatki),
                    		    success: function (res) {
                    		        callback(ehrId);
                    		    },
                    		    error: function(err) {
                    		    	$("#generiranjeObvestilo").html("<span class='obvestilo label " +
                                    "label-danger fade-in'>Napaka pri generiranju testnih primerov!</span>");
                                    genError = true;
                    		    }
                    		});
    	                }
    	            },
    	            error: function(err) {
    	            	$("#generiranjeObvestilo").html("<span class='obvestilo label " +
                        "label-danger fade-in'>Napaka pri generiranju testnih primerov!</span>");
                        genError = true;
    	            }
    	        });
    		}
    	});
	
    }, 500);
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function kreirajEHR() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var spol = $("#kreirajSpol").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
	    
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            gender: spol,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{
		                key: "ehrId", 
		                value: ehrId
		            }]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka!</span>");
		            }
		        });
		    }
		});
	}
}

function dodajMeritveVIT() {
	var sessionId = getSessionId();

	var ehrId = $("#dodajVITEHR").val();
	var datumInUra = $("#dodajVITDatumInUra").val();
	var telesnaVisina = $("#dodajTelesnaVisina").val();
	var telesnaTeza = $("#dodajTelesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVisineInTezeSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVisineInTezeSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>Uspešno dodani podatki o višini in teži.</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVisineInTezeSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka!</span>");
		    }
		});
	}
}

function dodajMeritveKT() {
	var sessionId = getSessionId();

	var ehrId = $("#dodajKTEHR").val();
	var datumInUra = $("#dodajKTDatumInUra").val();
	var sistolicniKrvniTlak = $("#dodajKrvniTlakSistolicni").val();
    var diastolicniKrvniTlak = $("#dodajKrvniTlakDiastolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveKrvniTlakSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveKrvniTlakSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>Uspešno dodani podatki o krvnem tlaku.</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveKrvniTlakSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka!</span>");
		    }
		});
	}
}

function prikazVIT(ehrId, callback) {
    var sessionId = getSessionId();
    var height = 0;
    var weight = 0;

    $.ajax({
  		url: baseUrl + "/view/" + ehrId + "/" + "height",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
		   	if (res.length > 0) {
		    	for (var i in res) {
		            height = res[i].height;
		            $("#prikazVisina").text(height + " " + res[i].unit);
		            break;
                }
		    } else {
		    	$("#prikazVisina").text("N.A.");
		    }
		},
	    error: function() {
	        $("#prikazVisina").text("Napaka!");
        }
    });
    
    $.ajax({
  		url: baseUrl + "/view/" + ehrId + "/" + "weight",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
		   	if (res.length > 0) {
		        for (var i in res) {
		            weight = res[i].weight;
		            $("#prikazTeza").text(weight + " " + res[i].unit);
		            break;
                }
		    } else {
		    	$("#prikazTeza").text("N.A.");
		    }
		},
	    error: function() {
	       $("#prikazTeza").text("Napaka!");
        }
    });
    
    
    setTimeout(function (){

    if(height == 0 || weight == 0)
        $("#prikazBMI").text("N.A.");
    else
        $("#prikazBMI").text((weight*10000/(height*height)).toFixed(2));

    }, 500);
}

function prikazKT(ehrId) {
    var sessionId = getSessionId();
    
    $.ajax({
  		url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (res) {
		   	if (res.length > 0) {
		    	for (var i in res) {
		            $("#prikazSisTlak").text(res[i].systolic + " mm Hg");
		            $("#prikazDiaTlak").text(res[i].diastolic + " mm Hg");
		            break;
                }
		    } else {
		    	 $("#prikazSisTlak").text("N.A.");
		         $("#prikazDiaTlak").text("N.A.");
		    }
		},
	    error: function() {
	        $("#prikazSisTlak").text("Napaka!");
		    $("#prikazDiaTlak").text("Napaka!");
        }
    });
}

function prikaziPodatke() {
    var sessionId = getSessionId();

	var ehrId = $("#prikazEhrID").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#prikazSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#prikazSporocilo").html("<span class='obvestilo label " +
                "Uspesno pridobljeni podtki.</span>");
                $("#prikazIme").text(party.firstNames + ' ' + party.lastNames);
                switch(party.gender) {
                    case "MALE":
                        $("#prikazSpol").text("Moški");
                        break;
                    case "FEMALE":
                        $("#prikazSpol").text("Ženska");
                        break;
                    case "OTHER":
                        $("#prikazSpol").text("Drugo");
                        break;
                    case "UNKNOWN":
                        $("#prikazSpol").text("Neznano");
                        break;
                }
                var dob = new Date(party.dateOfBirth);
                var today = new Date();
                var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                $("#prikazStarost").text(age);
                prikazVIT(ehrId);
                prikazKT(ehrId);
			},
			error: function(err) {
				$("#prikazSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka!</span>");
			}
        });
    }
}

function narisiGraf() {

    var ctx = document.getElementById("BMIChart");
    var BMIchart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: letnicePodatki,
            datasets: [{
                label: "BMI",
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(51,122,183,0.4)",
                borderColor: "rgba(51,122,183,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(51,122,183,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 5,
                pointHoverRadius: 7,
                pointHoverBackgroundColor: "rgba(51,122,183,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: podatki,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Trend BMI v Sloveniji'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
        	 }]
    	}
    	}
	});
}

var podatki = [];
var letnicePodatki = [];

function preberiJsonGraf(callback) {

    $.getJSON("data.json", function(json) {
        for (var i in json.fact) {

            if (parseInt(json.fact[i].dims["YEAR"]) % 5 == 0 && json.fact[i].dims["SEX"] === "Both sexes") {
                podatki.push(json.fact[i].Value.substring(0, 4));
                letnicePodatki.push(json.fact[i].dims["YEAR"]);
            }
        }

        podatki = podatki.reverse();
        letnicePodatki = letnicePodatki.reverse();
        callback();
    });

}


$(document).ready(function() {
    
    preberiJsonGraf(function() {
        narisiGraf();
    });
    
    document.getElementById("genBtn").addEventListener ("click", function() {
        genError = false;
        generirajPodatke(1, function(ehrId1) {
            generirajPodatke(2, function(ehrId2) {
                generirajPodatke(3, function(ehrId3){
                    $("#preberiPredlogo").html("<option value=''></option>" + "<option value='" + ehrId1 + "'>Mija Novak</option>" +
                        "<option value='" + ehrId2 + "'>Franc Petek</option>" + "<option value='" + ehrId3 + "'>Marjeta Golob</option>");
                    if(!genError)
                        $("#generiranjeObvestilo").html("<span class='obvestilo label " +
                        "label-success fade-in'>Uspešno generiranje testnih primerov.</span>");
                });
            });
        });
    }, false);
    document.getElementById("kreirajBtn").addEventListener ("click", kreirajEHR, false);
    document.getElementById("dodajVITbtn").addEventListener ("click", dodajMeritveVIT, false);
    document.getElementById("dodajKTbtn").addEventListener ("click", dodajMeritveKT, false);
    document.getElementById("prikaziBtn").addEventListener ("click", prikaziPodatke, false);
    
    $("#preberiPredlogo").change(function() {
        var changedEhrId = $("#preberiPredlogo").val();
        $("#prikazEhrID").val(changedEhrId);
    });
});